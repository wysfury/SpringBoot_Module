package cn.xiangxu.moduledemo.model;

        import lombok.Data;

/**
 * @author 王杨帅
 * @create 2018-07-26 22:16
 * @desc 学生实体类
 **/
@Data
public class Student {
    private Integer id;
    private String name;
}

