package cn.xiangxu.moduledemo.repository;

import cn.xiangxu.moduledemo.model.Student;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 王杨帅
 * @create 2018-07-26 22:17
 * @desc 学生持久层
 **/
@Repository
public class StudentRepository {

    /**
     * 利用Map模拟内存数据库
     */
    private final ConcurrentMap<Integer, Student> repository =new ConcurrentHashMap<>();

    /**
     * 主键发生器
     */
    private final static AtomicInteger idGenerator = new AtomicInteger();

    /**
     * 新增
     * @param student
     * @return
     */
    public Student save(Student student) {
        // 产生ID
        Integer id = idGenerator.incrementAndGet();
        student.setId(id);
        student.setName(student.getName() + id);
        repository.put(id, student);
        return student;
    }

    /**
     * 获取列表
     * @return
     */
    public Collection<Student> findAll() {
        return repository.values();
    }



}

