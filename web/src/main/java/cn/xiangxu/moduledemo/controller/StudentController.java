package cn.xiangxu.moduledemo.controller;

import cn.xiangxu.moduledemo.model.Student;
import cn.xiangxu.moduledemo.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * @author 王杨帅
 * @create 2018-07-26 22:23
 * @desc 学生控制层
 **/
@RestController
@RequestMapping(value = "/student")
@Slf4j
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @GetMapping
    public Collection<Student> findAll() {
        return studentRepository.findAll();
    }

    @PostMapping
    public Student save(@RequestBody Student student) {
        return studentRepository.save(student);
    }

    @GetMapping(value = "/connect")
    public String connect() {
        String result = "前后端连接成功";
        log.info(result);
        return result;
    }

}

